#include <iostream>
#include <vector>
#include <string>
#include <math.h>

using namespace std;

#define INDEXNONEZERO 1

vector<int> reverse(vector<int> str)
{
    int n = str.size();
 
    for (int i=0; i < n/2; i++)
       swap(str[i], str[n-i-1]);

   return str;

}

vector<int> get_from_x_to_y(vector<int> s, int first, int last) {
	vector<int> temp;
	for (int i = first; i < last; i++) {
		temp.push_back(s[i]);
	}
	return temp;
}

vector<int> concatinate(vector<int> temp1, vector<int> temp2) {
	for(int i = 0; i < temp1.size(); i++) {
		temp2.push_back(temp1[i]);
	}
	return temp2;
}

vector<int> encoder(vector<int> s) {
	int k = floor(s.size() / 2);
	if(s.size() == 1) {
		return s;
	}

	vector<int> temp1, temp2;
	temp1 = get_from_x_to_y(s, 0, k);
	temp2 = get_from_x_to_y(s, k, s.size());

	temp1 = encoder(reverse(temp1));
	temp2 = encoder(reverse(temp2));

	temp2.reserve( temp1.size()); // preallocate memory

	return concatinate(temp1, temp2);
}

int find_index (vector<int> s, int m) {
	for(int i = 0; i < s.size(); i++) {
		if(s[i] == m) {
			return i + INDEXNONEZERO;
		}
	}
}

void print_debug (vector<int> indexes) {
	for(int i = 0; i < indexes.size(); i++) {
		cout << indexes[i];
	}
	cout << endl;
}

int main () {

	vector<int> indexes;
	int n, m;
	cin >> n >> m;
	for(int i = 1; i <= n; i++) 
		indexes.push_back(i);

	indexes = encoder(indexes);

	cout << find_index(indexes, m) << endl;
	
	return 0;
}