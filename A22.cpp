#include <iostream>
#include <vector>
#include <cmath>
#include <stdlib.h>
using namespace std;

struct Row {
	vector<int> row;	
};

struct School {
	vector<vector<int> > students;
};

School input(int n) {
	School school;
	Row r;
	int num;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cin >> num;
			r.row.push_back(num);
		}
		school.students.push_back(r.row);
		r.row.clear();
	}
	return school;
}

School ones(int n) {
	School school;
	Row r;
	int num;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			r.row.push_back(1);
		}
		school.students.push_back(r.row);
		r.row.clear();
	}
	return school;
}

void printSchool (School s) {
	for (int i = 0; i < s.students.size(); i++) {
		for (int j = 0; j < s.students[i].size(); j++) {
			cout << s.students[i][j] << "  ";
		}
		cout << endl;
	}
}

bool checkTalls (School in, School out, int i, int j) {
	int m = 0;
	if (i < out.students.size()-1 && out.students[i][j] < out.students[i+1][j]) {
		m++;
	}
	if (j < out.students[i].size()-1 && out.students[i][j] < out.students[i][j+1]) {
		m++;
	}
	if (i > 0 && out.students[i][j] < out.students[i-1][j]) {
		m++;
	}
	if (j > 0 && out.students[i][j] < out.students[i][j-1]) {
		m++;
	}

	return m == in.students[i][j];
}

bool checkMatrix (School in, School out) {
	for (int i = 0; i < in.students.size(); i++) {
		for (int j = 0; j < in.students[i].size(); j++) {
			if (!checkTalls(in, out, i, j)) {
				return false;
			}
		}
	}
	return true;
}

void makeMatrix (School in, School out, int i, int j) {
	if (checkTalls(in, out, i, j) && j < in.students[i].size()-1) {
		makeMatrix(in, out, i, j+1);
	} else if (checkTalls(in, out, i, j) && j == in.students[i].size()-1 && i < in.students.size()-1) {
		makeMatrix(in, out, i+1, 0);
	} else if (checkTalls(in, out, i, j) && j == in.students[i].size()-1 && i == in.students.size()-1) {
		printSchool(out);
		exit(0);
	}

	if (checkMatrix(in, out)) {
		printSchool(out);
		exit(0);
	}

	if (!checkTalls(in, out, i, j) && in.students[i][j] == 0) {
		out.students[i][j]++;	
	}

	if (checkTalls(in, out, i, j) && j < in.students[i].size()-1) {
		makeMatrix(in, out, i, j+1);
	} else if (checkTalls(in, out, i, j) && j == in.students[i].size()-1 && i < in.students.size()-1) {
		makeMatrix(in, out, i+1, 0);
	}
	
	if (!checkTalls(in, out, i, j) && j < out.students[i].size()-1) {
		out.students[i][j+1] += abs(out.students[i][j+1] - out.students[i][j]) + 1;	
	}

	if (checkTalls(in, out, i, j) && j < in.students[i].size()-1) {
		makeMatrix(in, out, i, j+1);
	} else if (checkTalls(in, out, i, j) && j == in.students[i].size()-1 && i < in.students.size()-1) {
		makeMatrix(in, out, i+1, 0);
	}

	if (!checkTalls(in, out, i, j) && i < out.students.size()-1) {
		out.students[i+1][j] += abs(out.students[i+1][j] - out.students[i][j]) + 1;
	}

	if (checkTalls(in, out, i, j) && j < in.students[i].size()-1) {
		makeMatrix(in, out, i, j+1);
	} else if (checkTalls(in, out, i, j) && j == in.students[i].size()-1 && i < in.students.size()-1) {
		makeMatrix(in, out, i+1, 0);
	}

	return;
}

int main() {
	School mySchool, ourBuildSchool;
	int n;
	cin >> n;

	ourBuildSchool = ones(n);
	mySchool = input(n);

	makeMatrix(mySchool, ourBuildSchool, 0, 0);

	//printSchool(mySchool);
	//printSchool(ourBuildSchool);

	/*if(checkTalls(mySchool, ourBuildSchool, 0, 0)) {
		cout << "yess" << endl;
	} else {
		cout << "No" << endl;
	}*/

	return 0;
}