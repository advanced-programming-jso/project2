#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
using namespace std;

#define Reg 1
#define AddProp 2
#define AddFr 3
#define SeeProp 4

int sum = 0;

struct Props {
	vector<string> propName;
	vector<string> propWorth;
	string propCat;
};

struct Friends {
	vector<string> friendName;
	string friendCat;
};

struct User {
	string name;
	vector<Props> props;
	vector<Friends> friends;
};

struct Users {
	vector<User> data;
};

vector<string> Parse (string s) {
  vector<string> sentenceTokens;

  istringstream iss(s);   
  copy(istream_iterator<string>(iss),
        istream_iterator<string>(),
        back_inserter(sentenceTokens));

  return sentenceTokens;
}

int findCommand (vector<string> s) {
	if (s[0] == "Register") 
		return 1;
	else if (s[1] == "‫‪AddProperty‬‬") 
		return 2;
	else if (s[1] == "AddFriend")
		return 3;
	else if (s[0] == "SeeProperty")
		return 4;
	else 
		return -1;
}

int findUser (Users u, string n) {
	for(int i = 0; i < u.data.size(); i++)
	{
		if (u.data[i].name == n)
			return i;
	}
	return -1;
}

int findCatProp(User u, string s) {
	for(int i = 0; i < u.props.size(); i++)
	{
		if (u.props[i].propCat == s)
			return i;
	}
	return -1;
}

int findCatFriend(User u, string s) {
	for(int i = 0; i < u.friends.size(); i++)
	{
		if (u.friends[i].friendCat == s)
			return i;
	}
	return -1;
}

User Register (string s) {
	User user;
	user.name = s;
	return user;
}

void addProp (User& u, vector<string> s) {
	Props p;
	int numCat = findCatProp(u, s[4]);
	if (numCat != -1) {
		u.props[numCat].propName.push_back(s[2]);
		u.props[numCat].propWorth.push_back(s[3]);
	} else {
		p.propName.push_back(s[2]);
		p.propWorth.push_back(s[3]);
		p.propCat = s[4];
		u.props.push_back(p);
	}
}

void addFriend (User& u, vector<string> s) {
	Friends f;
	int numCat = findCatFriend(u, s[3]);
	if (numCat != -1) {
		u.friends[numCat].friendName.push_back(s[2]);
	} else {
		f.friendName.push_back(s[2]);
		f.friendCat = s[3];
		u.friends.push_back(f);
	}
}

void printTab (int i) {
	for (int j = 0; j < i; j++) {
		cout << "\t";
	}
}

void printProp (vector<Props> p, int type, int tabNum) {
	if (p.size() < 1) {
		return;
	}

	if (p[0].propName.size() > 0 && type) {
		printTab(tabNum);
		cout << "and ";
	}

	cout << "a list(" << endl;
	printTab(tabNum);
	cout << "\twhich includes ";
	for (int i = 0; i < p[0].propName.size(); i++) {
		if (p[0].propName.size() > 1)
			cout << "a ";
		sum += atoi(p[0].propWorth[i].c_str());
		cout << p[0].propName[i] << " worth " << p[0].propWorth[i] << " toman";
		if (p[0].propName.size() > 1 && p[0].propName.size()-1 != i)
			cout << " and ";
	}
	cout << ") as " << p[0].propCat << endl;

	p.erase(p.begin());
	printProp(p, 1, tabNum);
}

void printFriendProp (vector<Props> p, string s, int tabNum) {
	if (p.size() < 1) {
		return;
	}
	cout  << s << "(" << endl;
	printTab(tabNum+1);
	cout << "\twho has ";
	printProp(p, 0, tabNum+2);
}

void closingParanthesisHandler (int j, int tabNum, string cat) {
	for (int i = 0; i < j; i++) {
		printTab(tabNum);
		cout << ")" << endl;
	}
	printTab(tabNum-1);
	cout << ") as " << cat << endl;
}

void printFriends (Users u, int id, int friendNum) {
	if (u.data[id].friends.size() < 1) {
		return;
	}

	for (int i = 0; i < u.data[id].friends.size(); i++) {
		int friendNumLocal = friendNum;
		printTab(friendNum);
		cout << "and a list(" << endl;
		printTab(friendNum);
		cout << "\twhich includes ";
		for (int j = 0; j < u.data[id].friends[i].friendName.size(); j++) {
			printFriendProp(u.data[findUser(u, u.data[id].friends[i].friendName[j])].props, u.data[id].friends[i].friendName[j], friendNum);
			friendNumLocal+=2;
			printFriends(u, findUser(u, u.data[id].friends[i].friendName[j]), friendNum+2);
			if (j < u.data[id].friends[i].friendName.size()-1) {
				printTab(friendNum+1);
				cout << "and ";
			}
		}
		closingParanthesisHandler(u.data[id].friends[i].friendName.size(), friendNum+2, u.data[id].friends[i].friendCat);
	}
}

void seeProp (Users u, string s) {
	int userTableNum = findUser(u, s);
	cout << s << " has ";
	printProp(u.data[userTableNum].props, 0, 0);
	printFriends(u, userTableNum, 0);
	cout << "The total network of " << s << " is worth " << sum << " toman." << endl;
}

int main() {
	string sentence;
	vector<string> sentenceTokens;

	Users users;
	int i = 0;
	while (getline(cin, sentence)) {

		sentenceTokens = Parse(sentence);

		if(findCommand(sentenceTokens) == Reg){
			users.data.push_back(Register(sentenceTokens[1]));
		} else if (findCommand(sentenceTokens) == AddProp) {
			int userTableNum = findUser(users, sentenceTokens[0]);
			if (userTableNum != -1) {
				addProp(users.data[userTableNum], sentenceTokens);
			}
		} else if (findCommand(sentenceTokens) == AddFr) {
			int userTableNum = findUser(users, sentenceTokens[0]);
			if (userTableNum != -1) {
				addFriend(users.data[userTableNum], sentenceTokens);
			}
		} else if (findCommand(sentenceTokens) == SeeProp) {
			int userTableNum = findUser(users, sentenceTokens[1]);
			if (userTableNum != -1) {
				seeProp(users, sentenceTokens[1]);
			}
		} else {

		}
	}
	return 0;
}